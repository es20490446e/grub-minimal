#! /bin/bash
set -e

HERE="$(realpath "$(dirname "${0}")")"


SUPERUSER="$(
	if [[ "$(id -u)" -eq 0 ]]; then
		echo x
	fi
)"


ROOT="$(
	if [[ -z "${SUPERUSER}" ]]; then
		echo "${HERE}/_SIMULATED"
	fi
)"


GRUB_SMART="${ROOT}/etc/default/grub-smart"


post_install () {
	mkdir --parents "${GRUB_SMART}"
	cp "/etc/default/grub" "${GRUB_SMART}/old"
	cp "${GRUB_SMART}/default" "${ROOT}/etc/default/grub"
	UpdateGrub
}


post_upgrade () {
	if [[ -n "${ROOT}" ]]; then
		mkdir --parents "${ROOT}"
	fi

	cp --force "${GRUB_SMART}/default" "${ROOT}/etc/default/grub"
	UpdateGrub
	rm --force "${ROOT}/boot/grub/fonts/unicode-big.pf2"
}


pre_remove () {
	RevertGrubConfs
	mv --force "${GRUB_SMART}/old" "${ROOT}/etc/default/grub"
	UpdateGrub
}


RevertGrubConfs () {
	if [[ -n "${ROOT}" ]]; then
		mkdir --parents "${ROOT}"
	fi

	RevertGrubCredentials
	RevertGrubUnrestricted
}


RevertGrubCredentials () {
	sed \
		-e '/set superusers=admin/d' \
		-e '/password admin/d' \
		-e '/export superusers/d' /etc/grub.d/40_custom |
	head --bytes=-1 \
	> "${ROOT}/etc/grub.d/40_custom"

	chmod "u=rwx,g=r,o=r" "${ROOT}/etc/grub.d/40_custom"
}


RevertGrubUnrestricted () {
	local conf

	while read -r conf; do
		sed 's|[[:blank:]]--unrestricted||g' "${conf}" > "${ROOT}${conf}"
	done < <(
		find "/etc/grub.d" -type f
	)
}


UpdateGrub () {
	grub-mkconfig --output="${ROOT}/boot/grub/grub.cfg"
}
